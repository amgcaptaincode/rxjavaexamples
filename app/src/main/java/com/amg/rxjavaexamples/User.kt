package com.amg.rxjavaexamples

data class User(var name: String? = null, var gender: String? = null, var email: String? = null, var address: Address? = null)