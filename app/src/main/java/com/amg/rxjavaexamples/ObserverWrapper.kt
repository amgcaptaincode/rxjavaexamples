package com.amg.rxjavaexamples

import android.util.Log
import io.reactivex.Observer
import io.reactivex.disposables.Disposable


abstract class ObserverWrapper<T> : Observer<T> {

    override fun onNext(t: T) {
        onNextItem(t)
    }

    override fun onError(e: Throwable) {
        Log.e("ObserverWrapper", "onError", e)
    }

    override fun onComplete() {
        Log.d("ObserverWrapper", "All items emitted!")
    }

    protected abstract fun onNextItem(t: T)
}