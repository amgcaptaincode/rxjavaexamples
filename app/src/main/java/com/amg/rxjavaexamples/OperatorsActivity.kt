package com.amg.rxjavaexamples

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.ObservableSource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.random.Random

class OperatorsActivity : AppCompatActivity() {

    private val TAG = OperatorsActivity::class.java.simpleName
    private var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //operatorMap()

        //operatorFlatMap()

        //operatorConcatMap()

        operatorSwitchMap()

    }

    private fun operatorMap() {
        getUserObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                // modifying user object by adding email address
                // turing user name to uppercase
                it.email = "${it.name}@rxjava.wtf"
                it.name = it.name?.toUpperCase(Locale.getDefault())
                it
            }
            .subscribe(genericObserver())
    }

    /*
    * Assume this methods is making a network call and fetching Users
    * an Observable that emits list of users
    * each User has name and email, bit missing email id */

    private fun getUserObservable(): Observable<User> {

        val names = arrayOf("mark", "john", "trump", "obama")

        val users = arrayListOf<User>()

        names.forEach {
            val user = User()
            user.name = it
            user.gender = "male"
            users.add(user)
        }

        return Observable.create { emitter ->
            users.forEach {
                if (!emitter.isDisposed) {
                    emitter.onNext(it)
                }
            }

            if (!emitter.isDisposed) {
                emitter.onComplete()
            }
        }
    }

    private fun genericObserver(): ObserverWrapper<User> {
        return object : ObserverWrapper<User>() {
            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onNextItem(user: User) {
                Log.d(TAG, "onNext: ${user.name}, ${user.gender}, ${user.email}, ${user.address?.address}")
            }
        }
    }

    private fun operatorFlatMap() {

        getUserObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap {
                // getting each user address by making another network call
                return@flatMap getAddressObservable(it)
            }
            .subscribe(genericObserver())

    }

    /*
    * Assume this as a network call
    * returns Users with address filed added
    * */

    private fun getAddressObservable(user: User): Observable<User> {

        val addresses = arrayOf(
            "1600 Amphitheatre Parkway, Mountain View, CA 94043",
            "2300 Traverwood Dr. Ann Arbor, MI 48105",
            "500 W 2nd St Suite 2900 Austin, TX 78701",
            "355 Main Street Cambridge, MA 02142"
        )

        return Observable.create(ObservableOnSubscribe<User> { emitter ->
            val address = Address()
            address.address = addresses[Random.nextInt(2) + 0]
            if (!emitter.isDisposed) {
                user.address = address

                // Generate network latency of random duration
                val sleepTime = Random.nextInt(3000) + 500

                Thread.sleep(sleepTime.toLong())
                emitter.onNext(user)
                emitter.onComplete()
            }
        }).subscribeOn(Schedulers.io())
    }

    private fun operatorConcatMap() {
        getUserObservable()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .concatMap {
                return@concatMap getAddressObservable(it)
            }.subscribe(genericObserver())
    }

    private fun operatorSwitchMap() {
        //val integerObservable = Observable.fromArray(intArrayOf(1, 2, 3, 4, 5, 6))
        val integerObservable = Observable.fromArray(1, 2, 3, 4, 5, 6)

        // it always emits 6 as it un-subscribes the before observer
        integerObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .switchMap { integer -> Observable.just(integer).delay(1, TimeUnit.SECONDS) }
            .subscribe(object : ObserverWrapper<Int>() {

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onNextItem(t: Int) {
                    Log.d(TAG, "onNext: $t")
                }

            })
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

}